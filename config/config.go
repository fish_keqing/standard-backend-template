package config

var Conf = Config{}

type Config struct {
	Listen string `yaml:"listen"`
}

//root:123456@tcp(127.0.0.1:3306)/miyofun?charset=utf8mb4&parseTime=true
//type MysqlConf struct {
//	Username string `yaml:"username"`
//	Password string `yaml:"password"`
//	Ip       string `yaml:"ip"`
//	Port     string `yaml:"port"`
//	Database string `yaml:"database"`
//}
