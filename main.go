package main

import (
	"log"
	"os"
	"server/config"
	"server/router"
	"server/utils"

	"github.com/gin-contrib/gzip"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
)

func main() {
	var err error
	var c = &config.Conf
	var f *os.File
	f, err = os.OpenFile("./runtime.log", os.O_CREATE|os.O_APPEND, 0777)
	if err != nil {
		log.Panicln(err)
	}
	defer f.Close()
	log.SetOutput(f)
	err = utils.LoadYAML(c, "./server.yaml")
	if err != nil {
		log.Panicln(err)
	}
	server := gin.New()
	server.Use(gzip.Gzip(gzip.DefaultCompression))
	server.Use(static.Serve("/", static.LocalFile("./html/dist", true)))

	server.GET("/", func(context *gin.Context) {
		context.HTML(200, "index.html", nil)
	})
	router.InitRouter(server)
	_ = server.Run(c.Listen)
}
