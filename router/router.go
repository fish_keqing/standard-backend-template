package router

import (
	"server/controls"

	"github.com/gin-gonic/gin"
)

func InitRouter(r *gin.Engine) {
	RouterOne := r.Group("/api/convert")
	{
		RouterOne.POST("/webp", controls.ToWEBP)
	}
}
