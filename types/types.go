package types

type ReqToWEBP struct {
	Lossless bool    `form:"lossless"`
	Quality  float32 `form:"quality"`
	Exact    bool    `form:"exact"`
}
