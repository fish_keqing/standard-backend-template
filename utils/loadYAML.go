package utils

import (
	"gopkg.in/yaml.v3"
	"os"
	"server/config"
)

func LoadYAML(c *config.Config, address string) (err error) {
	f, err := os.Open(address)
	if err != nil {
		return
	}
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&c)
	if err != nil {
		return
	}
	_ = f.Close()
	return
}
